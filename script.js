
//Nested array with the information.
var infoList = [["Nancy", "Minneapolis, MN", "612-597-6570", "xiongn@augsburg.edu"],	
				["Najma", "Minneapolis, MN", "612-232-9360", "warsamen@augsburg.edu"],
				["Ramon", "South Saint Paul, MN", "651-802-6155", "orella14@augsburg.edu"]];
//nodes of the elements.				
var nameList = document.getElementsByTagName("li");			
var inputList = document.getElementsByTagName("input");		
var infoDisplayed = document.getElementsByTagName("p");		
var infoBackground = document.getElementById("info");
var spotclicked = -1;		//which spot in the info list was selected
var editing = false;		//save the editing
var adding = false;			//save the adding

//to find the index of a certain node in "name list"
function index(el) {
        i = 0;
    for (; i < nameList.length; i++) {
        if (nameList[i] == el) {
            return i;
        }
    }
    return -1;
}

//updates names.
function updateNames(){
	
	for(var i = 0; i < nameList.length; i++){
		nameList[i].innerHTML = infoList[i][0]
	}
}

//Once a name is clicked this retrieves the corresponding information.
function nameClick(thisElement){
	if(!editing && !adding)
	{
		spotclicked = index(thisElement);
		document.getElementById("btn").style.visibility = "visible"
		for(var j= 0; j < inputList.length; j++)
		{
			infoDisplayed[j].innerHTML = infoList[index(thisElement)][j];
			inputList[j].style.visibility = "hidden";
			infoDisplayed[j].style.visibility = "visible";
		}
	}
	else
	{
		alert("PLEASE SAVE OR CANCEL YOUR CHANGES");
	}
	console.log(spotclicked);
}


function startup(){
	updateNames();
	//hide all the input text/buttons
	for (var i = 0; i < inputList.length; i++){
		inputList[i].style.visibility = "hidden";
	}
	//hide the edit, save, and cancel button at first
	document.getElementById("btn").style.visibility = "hidden";
	document.getElementById("saveBtn").style.visibility = "hidden";
	document.getElementById("cancelBtn").style.visibility = "hidden";
}

//edit
function edit(thisElement){
	if(spotclicked != -1){
		//set editing to true, hide the edit button, show save and cancel
		editing = true;
		thisElement.style.visibility = "hidden";
		document.getElementById("saveBtn").style.visibility = "visible";
		document.getElementById("cancelBtn").style.visibility = "visible";
		//display the input boxes with the information of the name currently selected
		for(var i = 0; i < inputList.length; i++)
		{
			inputList[i].value = infoDisplayed[i].innerHTML;
			inputList[i].style.visibility = "visible";
			infoDisplayed[i].style.visibility = "hidden";
		}
	}
}

//save buttons
function save(thisElement){
	
	//validate that every input was filled out
	if(inputList[0].value != "" && 
	   inputList[1].value != "" && 
	   inputList[2].value != "" && 
	   inputList[3].value != "")
	{
		if(editing){
			//if editing, save all the values in the in input boxes, into the current selected spot
			for(var i = 0; i < inputList.length; i++)
			{
				inputList[i].style.visibility = "hidden";
				infoList[spotclicked][i] = inputList[i].value;
				infoDisplayed[i].innerHTML = inputList[i].value;
				infoDisplayed[i].style.visibility = "visible";
			}
			//hide the save and cancel buttons, show the edit an add
			document.getElementById("btn").style.visibility = "visible"
			document.getElementById("saveBtn").style.visibility = "hidden"
			document.getElementById("cancelBtn").style.visibility = "hidden"
			document.getElementById("addBtn").style.visibility = "visible"
		editing = false;
		adding = false;
		updateNames();
		}
		
		if(adding){
			//if adding, add new information to the info list based on what was put into the input spots
			infoList.push([inputList[0].value,
							inputList[1].value,
							inputList[2].value,
							inputList[3].value]);
			//create a new name for the list with a new li element
			var newElement = document.createElement("li")
			document.getElementById("list").appendChild(newElement);
			//hide/show correct buttons
			document.getElementById("btn").style.visibility = "visible"
			document.getElementById("saveBtn").style.visibility = "hidden"
			document.getElementById("cancelBtn").style.visibility = "hidden"
			document.getElementById("addBtn").style.visibility = "visible"
			//display the info of the new person added
			for(var j= 0; j < inputList.length; j++)
			{
				infoDisplayed[j].innerHTML = infoList[index(newElement)][j];
				inputList[j].style.visibility = "hidden";
				infoDisplayed[j].style.visibility = "visible";
			}
			//add a new event listener to the new name so it can display its correct information
			newElement.innerHTML = inputList[0].value;
			newElement.onclick = function(){
				spotclicked = index(this);
				for(var j= 0; j < inputList.length; j++)
				{
					infoDisplayed[j].innerHTML = infoList[index(newElement)][j];
					inputList[j].style.visibility = "hidden";
					infoDisplayed[j].style.visibility = "visible";
				}
			};
		}
		editing = false;
		adding = false;
		updateNames();
	}
	else
		alert("Please fill in all required fields");
}

//cancel button
function cancel(thisElement){
	
	//hide input fields, show the last information selected
	for(var i = 0; i < inputList.length; i++)
	{
		inputList[i].style.visibility = "hidden";
		infoDisplayed[i].style.visibility = "visible";
	}
	//if nothing was slelected before, don't display anything
	if (spotclicked != -1)
	{
	document.getElementById("btn").style.visibility = "visible"
	}
	//display/hide correct buttons
	document.getElementById("addBtn").style.visibility = "visible"
	document.getElementById("saveBtn").style.visibility = "hidden"
	document.getElementById("cancelBtn").style.visibility = "hidden"
	
	editing = false;
	adding = false;
}

//add button
function add(thisElement){

	adding = true;
	//display/hide correct buttons
	thisElement.style.visibility = "hidden";
	document.getElementById("saveBtn").style.visibility = "visible";
	document.getElementById("cancelBtn").style.visibility = "visible";
	document.getElementById("btn").style.visibility = "hidden";
	
	//display all the input boxes
	for(var i = 0; i < inputList.length; i++)
	{
		inputList[i].style.visibility = "visible";
		inputList[i].value = "";
		infoDisplayed[i].style.visibility = "hidden";
	}
}

